# Shiny skeleton app
This app is a minimal dashboard app that reads data from Socrata

# Configurable parameters

* DATA_SRC_URL :: This is an URL to a Socrata dataset. Any URL format
  supported by RSocrata wil
  work. (e.g. https://opendata.socrata.com/resource/77jn-2ym9.csv)
* PLOT_COL :: The name of a numeric column (e.g. 'mag' as per the
  above earthquake dataset example)
